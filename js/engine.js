
$(function() {


// Фиксированное меню при прокрутке страницы вниз

// $(window).scroll(function(){
//       var sticky = $('.header:not(.home .header)'),
//           scroll = $(window).scrollTop();
//       if (scroll > 200) {
//           sticky.addClass('header-fixed');
//       } else {
//           sticky.removeClass('header-fixed');
//       };
//   });



$(window).on('load', function() {
 $('.banner').css('height', '460px').delay(2000);
 setTimeout(function(){
  $('.banner h1').addClass('fadeInBottom');
}, 1000);

});


// FansyBox
 $('.fancybox').fancybox({});


// Сллайдер сертификатов
$('.sertificate-slider').slick({
  dots: false,
  arrows: true,
  speed: 300,
  slidesToShow: 6,
  slidesToScroll: 1,
  // autoplay: true,
  // autoplaySpeed: 5000,
  responsive: [
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 3,
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1
      }
    }
  ]
});


// Сллайдер клиентов
$('.project-list__slider').slick({
  dots: false,
  arrows: true,
  speed: 300,
  slidesToShow: 5,
  slidesToScroll: 1,
  // autoplay: true,
  // autoplaySpeed: 5000,
  responsive: [
    {
      breakpoint: 1599,
      settings: {
        slidesToShow: 4,
      }
    },
    {
      breakpoint: 1199,
      settings: {
        slidesToShow: 3,
      }
    },
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 2
      }
    },
    {
      breakpoint: 550,
      settings: {
        slidesToShow: 1
      }
    }
  ]
});




// Сллайдер проектов
var projectSlider = $('.project-slider').slick({
  dots: false,
  arrows: false,
  speed: 300,
  slidesToShow: 1,
  slidesToScroll: 1,
  // autoplay: true,
  // autoplaySpeed: 5000,
  centerMode: true,
  centerPadding: '420px',
  responsive: [
    {
      breakpoint: 1919,
      settings: {
        centerPadding: '300px',
      }
    },
    {
      breakpoint: 1599,
      settings: {
        centerPadding: '200px',
      }
    },
    {
      breakpoint: 1368,
      settings: {
        centerPadding: '140px',
      }
    },
    {
      breakpoint: 1199,
      settings: {
        centerPadding: '200px',
      }
    },
    {
      breakpoint: 991,
      settings: {
        centerPadding: '100px',
      }
    },
    {
      breakpoint: 550,
      settings: {
        centerPadding: '50px',
      }
    }
  ]
});
$('.project-slider__prev').click(function(){
    $(projectSlider).slick("slickPrev")
});
$('.project-slider__next').click(function(){
    $(projectSlider).slick("slickNext")
});






// Фиксированное меню при прокрутке страницы вниз
if ($(window).width() >= '991'){
$(window).scroll(function(){
        var sticky = $('.header'),
            scroll = $(window).scrollTop();
        if (scroll > 200) {
            sticky.addClass('header-nav__fixed');
        } else {
            sticky.removeClass('header-nav__fixed');
        };
    });
}

// Бургер меню
$(".burger-menu").click(function(){
  $(this).toggleClass("active");
  $('.header').toggleClass("active");
  $('.main-menu').fadeToggle(200);
});



// Беграунд
var list = document.querySelectorAll("div[data-image]");
for (var i = 0; i < list.length; i++) {
  var url = list[i].getAttribute('data-image');
  list[i].style.backgroundImage="url('" + url + "')";
}




$('.faq-header').click(function(){
  if(!($(this).next().hasClass('active'))){
    $('.faq-body').slideUp().removeClass('active');
    $(this).next().slideDown().addClass('active');
  }else{
    $('.faq-body').slideUp().removeClass('active');
  };

  if(!($(this).parent('.faq-item').hasClass('active'))){
    $('.faq-item').removeClass("active");
    $(this).parent('.faq-item').addClass("active");
  }else{
    $(this).parent('.faq-item').removeClass("active");
  };
});








})